<?php
/*
Plugin Name: vBSSO Checker
Plugin URI: http://URI_Of_Page_Describing_Plugin_and_Updates
Description: The WordPress plugin to check if vBSSO installed properly on user's platform
Version: 1.0
Author: Deyv Horni
*/

/**
 * @author Deyv Horni
 * Created at 01.12.18 8:03
 */
new VBSSOChecker();
define('VBSSO_CHECK_PLUGIN_DIR', dirname(__FILE__));
define('VBSSO_CHECK_PLUGIN_URL', WP_PLUGIN_URL . '/' . basename(__DIR__));
register_uninstall_hook(__FILE__, ['VBSSOChecker', 'uninstall']);

class VBSSOChecker
{
	private static $options = [];

	public function __construct() {
		self::$options = [
			'path' => get_option('vbsso_checker_path'),
			'isSave' => get_option('vbsso_checker_isSave'),
		];
		$this->includeFiles();
		add_action('init', [$this, 'init']);
		if (is_admin()) {
			add_action('init', [$this, 'initAdmin']);
		}
		add_action('widgets_init', [$this, 'initWidget']);
	}

	public function init() {
		if ( ! session_id()) {
			session_start();
		}
		add_option('vbsso_checker_path', '/plugins/' . basename(__DIR__) . '/results');
		add_option('vbsso_checker_isSave', '1');
		if (empty(get_option('vbsso_checker_path'))) {
			update_option('vbsso_checker_path', '/plugins/' . basename(__DIR__) . '/results');
		}
	}

	public function initAdmin() {
		new VBSSOCheckerAdmin();
	}

	public function initWidget() {
		register_widget('VBSSOCheckerWidget');
	}

	private function includeFiles() {
		require_once dirname(__FILE__) . '/VBSSOCheckerWidget.php';
		require_once dirname(__FILE__) . '/VBSSOCheckerFormData.php';
		require_once dirname(__FILE__) . '/VBSSOCheckerCaptcha.php';
		require_once dirname(__FILE__) . '/VBSSOCheckerAdmin.php';
	}

	public static function render(string $template, array $fields = []) {
		$path = dirname(__FILE__) . '/views/' . $template . '.php';
		extract($fields);

		ob_start();
		include $path;

		echo ob_get_clean();
	}

	public static function getPlatforms() {
		return [
			['suffix' => 'vbsso/vbsso.php?a=act', 'name' => 'vBulletin 4'],
			['suffix' => 'lib/plugins/vbsso/vbsso.php?action=vbsso', 'name' => 'Dokuwiki 2012 - 2017'],
			['suffix' => 'vbsso/1.0', 'name' => 'Drupal 7.37 - 8.3'],
		];
	}

	public static function getOptions() {
		return self::$options;
	}

	public static function uninstall() {
		if ( ! current_user_can('activate_plugins')) {
			return;
		}
		if (plugin_basename(__FILE__) !== WP_UNINSTALL_PLUGIN) {
			return;
		}

		delete_option('vbsso_checker_path');
		delete_option('vbsso_checker_isSave');
	}
}
