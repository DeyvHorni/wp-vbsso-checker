<?php
/**
 * @author Deyv Horni
 * Created at 01.12.18 9:05
 */
?>
<?=$args['before_widget']?>
<?=$args['before_title']?>
<?=$args['widget_name']?>
<?=$args['after_title']?>
<?php if ( ! empty($message)): ?>
	<?php if ($message instanceof stdClass): ?>
        <div class="message">
			<?=__('Version: ', 'vbsso-checker') . $message->version?><br />
			<?=__('Plugin version: ', 'vbsso-checker') . $message->plugin_version?><br />
			<?=__('PHP version: ', 'vbsso-checker') . $message->php?><br />
			<?=__('Memory limit: ', 'vbsso-checker') . $message->memory_limit?><br />
        </div>
	<?php else: ?>
        <div class="error">
		<?=$message?>
        </div>
	<?php endif; ?>
<?php endif; ?>
<form action="" method="POST">
    <laber for="vbsso_check_url">URL</laber>
    <input type="url" id="vbsso_check_url" name="vbsso_check_url" value="<?=$data->getUrl()?>" required>
    <select name="vbsso_check_suffix" id="vbsso_check_suffix">
		<?php foreach ($platforms as $key => $platform): ?>
            <option value="<?=$key?>"><?=$platform['name']?></option>
		<?php endforeach; ?>
    </select>
    <div class="captcha">
		<?=$captcha?><br />
    </div>
    <label for="vbsso_check_captcha">Enter text from image</label>
    <input type="text" name="vbsso_check_captcha" id="vbsso_check_captcha" required>
    <input type="submit" name="submit">
</form>
<?=$args['after_widget']?>
