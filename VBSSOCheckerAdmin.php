<?php

/**
 * @author Deyv Horni
 * Created at 04.12.18 19:23
 */
class VBSSOCheckerAdmin
{
	public function __construct() {
		add_action('admin_menu', [$this, 'addPage']);
		add_action('admin_init', [$this, 'registerSettings']);
	}

	public function addPage() {
		add_options_page(__('vBSSO Checker Settings', 'vbsso-checker'), __('vBSSO Checker', 'vbsso-checker'), 'manage_options', 'vbsso-checker', [
			$this,
			'viewSettings'
		]);
	}

	public function registerSettings() {
		register_setting('vbsso_checker_options_group', 'vbsso_checker_isSave');
		register_setting('vbsso_checker_options_group', 'vbsso_checker_path');
		$section = 'vbsso_checker_options';
		$page = 'vbsso-checker';
		add_settings_section($section, __('Main', 'vbsso-checker'), '', $page);
		add_settings_field(
			'vbsso_checker_isSave',
			__('Do save the results to a csv file?', 'vbsso-checker'),
			[$this, 'fieldisSave'],
			$page,
			$section,
			['laber_for' => 'vbsso_checker_isSave']
		);
		add_settings_field(
			'vbsso_checker_path',
			__('Path to reports folder (from the wp-content directory). Must start with "/"', 'vbsso-checker'),
			[$this, 'fieldPath'],
			$page,
			$section,
			['laber_for' => 'vbsso_checker_path']
		);
	}

	public function viewSettings() {
		?>
		<div class="wrap">
			<h1><?php __('vBSSO Checker Settings', 'vbsso-checker') ?></h1>
			<form action="options.php" method="post">
				<?php settings_fields('vbsso_checker_options_group') ?>
				<?php do_settings_sections('vbsso-checker') ?>
				<?php submit_button() ?>
			</form>
		</div>
		<?php
	}

	public function fieldPath() {
		$option = get_option('vbsso_checker_path');
		?>
		<p>
			<input type="text" name="vbsso_checker_path" id="vbsso_checker_path" value="<?=$option?>" class="">
		</p>
		<?php
	}

	public function fieldisSave() {
		$option = get_option('vbsso_checker_isSave');
		?>
		<p>
			<input type="hidden" name="vbsso_checker_isSave" id="vbsso_checker_isSave" value="0">
			<input type="checkbox" name="vbsso_checker_isSave" id="vbsso_checker_isSave" value="1" <?php if ($option == true) echo 'checked'?>>
		</p>
		<?php
	}
}
