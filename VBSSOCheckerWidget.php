<?php

/**
 * @author Deyv Horni
 * Created at 01.12.18 8:17
 */
class VBSSOCheckerWidget extends WP_Widget
{
	public function __construct() {
		parent::__construct('vBSSO-Checker', __('vBSSO Checker', 'vbsso-checker'), [
			'description' => __('Checked vBSSO installed on user\'s platform', 'vbsso-checker')
		]);
	}

	public function widget($args, $instance) {
		$message = null;
		$formData = new VBSSOCheckerFormData();
		$captcha = new VBSSOCheckerCaptcha();

		if ($formData->isSubmit()) {
			if ($formData->isValid()) {
				$response = $formData->sendRequestToPlatform();
				if ( ! is_array($response)) {
					$message = __('The request was completed with an error', 'vbsso-checker');
					$formData->saveResult($response, 'The request was completed with an error');
				} else {
					$message = $formData->parseResponse($response);
					$formData->saveResult($response['response']['code'], $message);
				}
			} else {
				$message = __('The form has been filled out incorrectly', 'vbsso-checker');
			}
		}
		VBSSOChecker::render('widget', [
			'message' => $message,
			'data' => $formData,
			'platforms' => $formData->getPlatforms(),
			'captcha' => $captcha->generateImage()->render(),
			'args' => $args
		]);
	}
}
