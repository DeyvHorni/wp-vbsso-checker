<?php

/**
 * @author Deyv Horni
 * Created at 03.12.18 13:32
 */
class VBSSOCheckerCaptcha
{
	private $width;
	private $height;

	public function __construct(string $font = null, int $width = 120, int $height = 38) {
		$this->width = $width;
		$this->height = $height;
	}

	public function generateImage() {
		$text = $this->generateText(6);
		$image = imagecreatetruecolor($this->width, $this->height);
		$bgColor = imagecolorallocate($image, 255, 255, 255);
		imagefilledrectangle($image, 0, 0, 200, 50, $bgColor);
		$this->addLines($image, 5);
		$this->addDots($image, 100);
		$this->addText($image, $text);
		$this->deleteOutdatedImages();
		imagepng($image, VBSSO_CHECK_PLUGIN_DIR . '/img/captcha_' . md5($text) . '.png');

		return $this;
	}

	public function render() {
		return '<img src="' . VBSSO_CHECK_PLUGIN_URL . '/img/captcha_' . $_SESSION['captcha'] . '.png">';
	}

	private function addLines($image, int $count) {
		$color = imagecolorallocate($image, 80, 80, 80);
		for ($i = 0; $i < $count; $i ++) {
			imageline($image, 0, mt_rand(0, $this->height), $this->width, mt_rand(0, $this->height), $color);
		}
	}

	private function addDots($image, int $count) {
		$color = imagecolorallocate($image, 255, 0, 100);
		for ($i = 0; $i < $count; $i ++) {
			imagesetpixel($image, mt_rand(0, $this->width), rand(0, $this->height), $color);
		}
	}

	private function addText($image, string $text) {
		$color = imagecolorallocate($image, 0, 0, 0);
		$lenght = strlen($text);
		for ($i = 0; $i < $lenght; $i ++) {
			imagestring($image, 15, 5 + ($i * 15), rand(5, 20), $text[$i], $color);
		}
	}

	private function generateText(int $lenght) {
		$text = null;
		$letters = 'abcdefghijkmnopqrstuvwxyz';
		$len = strlen($letters);
		for ($i = 0; $i < $lenght; $i ++) {
			$text .= $letters[rand(0, $len - 1)];
		}
		$_SESSION['captcha'] = md5($text);

		return $text;
	}

	public static function isValid(string $captcha) {
		if ( ! array_key_exists('captcha', $_SESSION) || empty($captcha) || md5($captcha) !== $_SESSION['captcha']) {
			return false;
		}

		return true;
	}

	private function deleteOutdatedImages() {
		$images = glob(VBSSO_CHECK_PLUGIN_DIR . '/img/captcha*.png');
		foreach ($images as $image) {
			unlink($image);
		}
	}
}
