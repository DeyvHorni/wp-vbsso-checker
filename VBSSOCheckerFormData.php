<?php

/**
 * @author Deyv Horni
 * Created at 03.12.18 10:13
 */
class VBSSOCheckerFormData
{
	private $url;
	private $platformKey;
	private $captcha;

	/**
	 * VBSSOCheckerFormData constructor.
	 *
	 * @param string $url
	 * @param int $suffix
	 */
	public function __construct() {
		$this->url = array_key_exists('vbsso_check_url', $_POST) ? $_POST['vbsso_check_url'] : null;
		$this->platformKey = array_key_exists('vbsso_check_suffix', $_POST) ? (int) $_POST['vbsso_check_suffix'] : null;
		$this->captcha = array_key_exists('vbsso_check_captcha', $_POST) ? $_POST['vbsso_check_captcha'] : null;
	}

	public static function isSubmit(): bool {
		if (array_key_exists('submit', $_POST)) {
			return true;
		}

		return false;
	}

	/**
	 * @return bool
	 */
	public function isValid() {
		if (!VBSSOCheckerCaptcha::isValid($this->captcha)) {
			return false;
		}
		if (empty($this->url) || !is_int($this->platformKey)) {
			return false;
		}

		return true;
	}

	public function sendRequestToPlatform() {
		$platform = $this->getPlatform($this->platformKey);
		$response = wp_remote_post(trailingslashit($this->url) . $platform['suffix'], ['timeout' => 45,]);
		if (is_wp_error($response)) {
			return $response->get_error_code();
		}
		if ($response['response']['code'] !== 200)
		{
			return $response['response']['code'];
		}

		return $response;
	}

	public function parseResponse($response) {
		$data = json_decode($response['body']);
		if ($data instanceof stdClass) {
			return $data;
		}

		return 'vBSSO is not installed or the completed form is incorrect.';
	}

	public function saveResult($statusCode, $data) {
		$options = VBSSOChecker::getOptions();
		if ($options['isSave'] == false) {
			return;
		}
		if ($data instanceof stdClass) {
			$data = json_encode($data);
		}
		$date = date('Ymd');
		$platform = $this->getPlatform($this->platformKey);
		$upload_dir = trailingslashit(WP_CONTENT_DIR . $options['path']);
		$fp = fopen("{$upload_dir}vbsso_verify_{$date}.csv", 'a');
		fputcsv($fp, [
			$this->url,
			$platform['name'],
			$statusCode,
			$data
		]);

		return fclose($fp);
	}

	public function getPlatforms() {
		return VBSSOChecker::getPlatforms();
	}

	private function getPlatform(int $key) {
		$platforms = $this->getPlatforms();

		return $platforms[$key];
	}

	public function getUrl() {
		return $this->url;
	}

	public function setUrl(string $url): self {
		$this->url = trailingslashit($url);

		return $this;
	}

	public function setplatformKey(int $key): self {
		$this->platformKey = $key;

		return $this;
	}
}
